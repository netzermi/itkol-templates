\babel@toc {ngerman}{}
\contentsline {chapter}{\nonumberline Abbildungsverzeichnis}{9}{chapter*.6}% 
\contentsline {chapter}{\nonumberline Tabellenverzeichnis}{10}{chapter*.7}% 
\contentsline {chapter}{\numberline {1}Einleitung}{11}{chapter.1}% 
\contentsline {chapter}{\numberline {2}Projektmanagement}{12}{chapter.2}% 
\contentsline {section}{\numberline {2.1}Metainformationen}{12}{section.2.1}% 
\contentsline {subsection}{\numberline {2.1.1}Projektteam}{12}{subsection.2.1.1}% 
\contentsline {subsection}{\numberline {2.1.2}Projektbetreuer}{13}{subsection.2.1.2}% 
\contentsline {subsection}{\numberline {2.1.3}Projektpartner}{13}{subsection.2.1.3}% 
\contentsline {section}{\numberline {2.2}Vorerhebungen}{13}{section.2.2}% 
\contentsline {subsection}{\numberline {2.2.1}Ist-Zustand}{13}{subsection.2.2.1}% 
\contentsline {subsection}{\numberline {2.2.2}Soll-Zustand}{14}{subsection.2.2.2}% 
\contentsline {subsection}{\numberline {2.2.3}Projektumfeldanalyse}{14}{subsection.2.2.3}% 
\contentsline {subsection}{\numberline {2.2.4}Risikoanalyse}{16}{subsection.2.2.4}% 
\contentsline {section}{\numberline {2.3}Pflichtenheft}{17}{section.2.3}% 
\contentsline {subsection}{\numberline {2.3.1}Zielsetzung}{18}{subsection.2.3.1}% 
\contentsline {subsection}{\numberline {2.3.2}Produkteinsatz}{18}{subsection.2.3.2}% 
\contentsline {subsection}{\numberline {2.3.3}Funktionalitäten}{19}{subsection.2.3.3}% 
\contentsline {subsubsection}{\nonumberline Funktionale Anforderungen}{19}{section*.13}% 
\contentsline {subsubsection}{\nonumberline Nicht-funktionale Anforderungen}{19}{section*.14}% 
\contentsline {subsection}{\numberline {2.3.4}Testszenarien und Testfälle}{20}{subsection.2.3.4}% 
\contentsline {subsection}{\numberline {2.3.5}Liefervereinbarungen}{21}{subsection.2.3.5}% 
\contentsline {section}{\numberline {2.4}Planung}{22}{section.2.4}% 
\contentsline {subsection}{\numberline {2.4.1}Projektstrukturplan}{22}{subsection.2.4.1}% 
\contentsline {subsection}{\numberline {2.4.2}Projektablaufplan}{23}{subsection.2.4.2}% 
\contentsline {subsection}{\numberline {2.4.3}Abnahmekriterien}{23}{subsection.2.4.3}% 
\contentsline {subsection}{\numberline {2.4.4}Evaluationsplan}{24}{subsection.2.4.4}% 
\contentsline {chapter}{\numberline {3}Vorstellung des Produkts}{25}{chapter.3}% 
\contentsline {chapter}{\numberline {4}Eingesetzte Technologien}{26}{chapter.4}% 
\contentsline {chapter}{\numberline {5}Problemanalyse}{27}{chapter.5}% 
\contentsline {section}{\numberline {5.1}Use-Case-Analyse}{27}{section.5.1}% 
\contentsline {section}{\numberline {5.2}Domain-Class-Modelling}{29}{section.5.2}% 
\contentsline {section}{\numberline {5.3}User-Interface Design}{29}{section.5.3}% 
\contentsline {chapter}{\numberline {6}Systementwurf}{30}{chapter.6}% 
\contentsline {section}{\numberline {6.1}Architektur}{30}{section.6.1}% 
\contentsline {subsection}{\numberline {6.1.1}Design der Komponenten}{30}{subsection.6.1.1}% 
\contentsline {subsection}{\numberline {6.1.2}Benutzerschnittstellen}{30}{subsection.6.1.2}% 
\contentsline {subsection}{\numberline {6.1.3}Datenhaltungskonzept}{31}{subsection.6.1.3}% 
\contentsline {subsection}{\numberline {6.1.4}Konzept für Ausnahmebehandlung}{31}{subsection.6.1.4}% 
\contentsline {subsection}{\numberline {6.1.5}Sicherheitskonzept}{31}{subsection.6.1.5}% 
\contentsline {subsection}{\numberline {6.1.6}Design der Testumgebung}{31}{subsection.6.1.6}% 
\contentsline {subsection}{\numberline {6.1.7}Design der Ausführungsumgebung}{31}{subsection.6.1.7}% 
\contentsline {section}{\numberline {6.2}Detailentwurf}{31}{section.6.2}% 
\contentsline {chapter}{\numberline {7}Implementierung}{33}{chapter.7}% 
\contentsline {chapter}{\numberline {8}Deployment}{34}{chapter.8}% 
\contentsline {chapter}{\numberline {9}Tests}{35}{chapter.9}% 
\contentsline {section}{\numberline {9.1}Systemtests}{35}{section.9.1}% 
\contentsline {section}{\numberline {9.2}Akzeptanztests}{35}{section.9.2}% 
\contentsline {chapter}{\numberline {10}Evaluation}{36}{chapter.10}% 
\contentsline {section}{\numberline {10.1}Projektevaluation}{36}{section.10.1}% 
\contentsline {section}{\numberline {10.2}Akzeptanztests}{36}{section.10.2}% 
\contentsline {section}{\numberline {10.3}Resümee}{36}{section.10.3}% 
\contentsline {chapter}{\numberline {11}Benutzerhandbuch}{37}{chapter.11}% 
\contentsline {chapter}{\numberline {12}Zusammenfassung}{38}{chapter.12}% 
\contentsline {chapter}{\nonumberline Literaturverzeichnis}{39}{chapter*.24}% 
\contentsline {chapter}{\numberline {A}Anhang}{40}{appendix.A}% 
